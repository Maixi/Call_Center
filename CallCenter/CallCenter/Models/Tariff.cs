﻿using System;

namespace CallCenter.Models
{
    public class Tariff
    {
        public Guid UId { get; set; }
        public string Name { get; set; }
        public string Bid { get; set; }
        public string Description { get; set; }
    }
}