﻿using System;

namespace CallCenter.Models
{
    public class Operator
    {
        public Guid UId { get; set; }
        public string Name { get; set; }
        public string ContactPhone { get; set; }        

    }
}